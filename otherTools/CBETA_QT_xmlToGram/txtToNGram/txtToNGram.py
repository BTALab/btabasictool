# -*- coding: utf-8 -*-
import sys
import codecs
import os
import shutil
import ngramtool
import getopt
import re
# 相容於python2, 本程式可於 python2/3 環境下執行
if (sys.version_info.major==2):  
	reload(sys)
	sys.setdefaultencoding('utf-8')
	
	
#處裡輸入值 -n
def readArgs(argv):
	try:
		opts, args = getopt.getopt(argv, "n:",["nojing","nojuan"])
	except getopt.GetoptError:
		print("usage: -n gramLength [--nojing] [--nojuan] (1)")
		sys.exit(2)
	
	if (len(opts) < 1): #沒有輸入參數
		print("usage: -n gramLength [--nojing] [--nojuan] (2)")
		sys.exit(2)

	v=None
	args=[]
	for arg,value in opts:
		if arg=="-n":
			try:
				v= int(value)
			except ValueError as e:
				print("error: gramLength is not integer \nusage: -n gramLength [--nojing] [--nojuan] (3)")
				sys.exit(2)
		elif arg=="--nojing":
			args.append("nojing")
		elif arg=="--nojuan":
			args.append("nojuan")

	if (v is None or v <=0):  
		print("error: -n is missing, usage: -n gramLength [--nojing] [--nojuan] (4)")
		sys.exit(2)
	else:
		return v,args

			
def main(args):
	
	dataDir='Jing_Txt_Data'
	outputDir='NGram_Output'
	Exclusive_DIR=[".git",".DontRemoveMe"]
	
	# filesTypes could be ["nojing","nojuan"]
	gLength,filesTypes=readArgs(args) #由參數列parse出gram長度的設定
	
	for root, subdirs, files in os.walk(dataDir,followlinks=True):
		print (root)
	#for f in os.listdir(dataDir):
		# Loop dictionary and get Filenames
		currentDir=os.path.split(root)[-1]
		subdirs[:] = [d for d in subdirs if d not in Exclusive_DIR]
		print(subdirs)

		for filename in files:
			
			print(filename)
			if filename[-4:]==".txt":
				if "nojing" in filesTypes and filename[-7:-4]=="000":
					print ("skip:{}".format(os.path.join(root,filename)))
					continue
					
				if "nojuan" in filesTypes and (not filename[-7:-4]=="000") and re.match("[0-9]{3}",filename[-7:-4]):
					print ("skip:{}".format(os.path.join(root,filename)))
					continue
				
				outFN=("%s%s%i%s") % (filename[:-4],"_GL",gLength,filename[-4:])
		
				outFileName=os.path.join(outputDir,outFN)
				
				if os.stat(os.path.join(root,filename)).st_size > 0:  # only doing Ngraming when input file is not empty
					fin = codecs.open('%s' % os.path.join(root,filename), 'r','utf8')

					#存放內容的List
					contentArray=[]    

					for line in fin:
						contentArray.append(line.strip()) #將每一行內容放入txtArray, 各行內容會被當作一個部份來處裡。
					
					print("*** 開始處裡：{%s}" % os.path.join(root,filename))
					
					g= ngramtool.VLNgram(contentArray)
					g.dumpTextArray(max=0)
					g.build_ngram(gramLength=gLength,sortBy="count",docThreshold=0,verbose=None) 
					
					#本程式應該一經一檔，所以每次執行，contentArray應該只有1個內容
					#因此，應該只有id=0的資料。
					grams=g.sortedItems(id=0)  #取得id=0的樣本,所有排序完成gram的資料。
					
					print("*** 輸出到：{%s}" % outFileName)
					fout = codecs.open('%s' % outFileName, 'w','utf8')
					
					for gram in grams:
						fout.write("%s,%i\r\n"%(gram.text,gram.count)), 
				
					fout.close()
				else:
					open(outFileName, 'a').close()  # create empty file
					
				noG=sum([ gram.count for gram in grams])
				
				print ("*** 共產生%i種不同gram,總數%i個" % (len(grams),noG))
	
if __name__ == '__main__':
	main(sys.argv[1:])
	
	
