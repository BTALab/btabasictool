txtToNGram -- 把去標文字檔內容，轉換為gram
-- 主程式：txtToNGram.py
-- 所需函式庫：ngramtool.py
-- 來源檔案位置：請置於 Jing_Txt_Data/ 當中
-- 輸出檔案位置：NGram_Output/

程式執行範例：
txtToNGram.py -n 2  (把來源內容產生長度為2的ngram切割)
txtToNGram.py -n 4  (把來源內容產生長度為4的ngram切割)

其他注意：
0. 長度可以為 >0 的任意值
1. 會循環處裡 Jing_Txt_Data/ 當中的所有檔案
2. 產生的結果檔案會為 ：原檔名_GL%gram長度設定%.txt
3. 建議寫批次檔處裡
