# -*- coding: utf-8 -*-
# 
#ver 0.1 (2013.01.29 by Joey Hung) 提供基本 固定n 的ngram功能, py2 相容
#ver 0.2 (2013.02.21 by Joey Hung) 提供以文件數目過濾gram的功能
#2013.02.21 開始整合VLNgram之功能
#2013.02.23 以gram 資料結構取代原有複雜tuple
# 2014.01.21 增加對於python3的相容性
# v0.3.2

import time

class gram:
	def __init__(self,text,count=1,docCount=1,pos=[]):
		""" 初始化函式 """
		self.text=text
		self.count=count
		self.docCount=docCount
		self.pos=pos
		
	def allstr(self):
		return "%s: %d(%d) [%s]"%(self.text,self.count,self.docCount,",".join([str(x) for x in self.pos]))
				
			
	def updateCount(self):
		self.count =sum(self.pos)
		self.docCount=sum([1 for x in self.pos if x>0])
		
	def __str__(self):
		return "%s: %d(%d)"%(self.text,self.count,self.docCount)
	
	def __lt__(self, other):
		return (self.text<other.text)
	
	def posSubtract(self,other):
		for i in range(len(self.pos)):
			if self.pos[i]>=other.pos[i]:
				self.pos[i]=self.pos[i]-other.pos[i]
			else:
				self.pos[i]=0
		self.updateCount()
		
class VLNgram:
	"""
		資料結構紀錄：
		self.srcTextArray : 原始字串array :: [str1,str2.....,strN]
		
		-- 各sample gram --
		self.gramsDict : 各字串轉成gramDict 後的陣列, [gramDict1,  gramDict2, gramDict3....]
				self.gramsDict[0] = gramDict1 = {gram1,gram2,gram3.........}
		self.grams : 各字串產生之gram, 按照 sortBy排序之後的 List: [gramSortedList1,gramSortedList2,gramSortedList3.....]
				self.grams[0] = gramSortedList1 = [gram1,gram2,gram3.........]
		
		--  整體gram --
		self.allGramsDict = gramDict_全部
		self.allGrams = gramSortedList_全部
		
	"""
	def __init__(self,textArr):
		self.srcTextArray=textArr[:]	#原始資料
		self.sampleSize=len(self.srcTextArray)

	def __getitem__(self,id):
		if id=="all":
			return self.allgramsDict
		else:
			return self.gramsDict[id]
	
	def sortedItems(self,id,max=None):
		if id=="all":
			return self.allGrams[:max]
		else:
			return self.grams[id][:max]
		
	def textDecomp(self,s,minLen):  #s: gram text, gramMinLen
		return [ s[i:i+n] for n in range(len(s)-1,minLen-1,-1) for i in range(len(s)-n+1)  ]
		
		
	def sort_dict_by_length(self,d):  #d 傳dictionary進來，傳回排完的list
		res=[g for g in d.values() ] # 由d 產生 key, value 的 tuple list
		return sorted(res,key=lambda x: len(x.text),reverse = True) 
		
	def sort_dict_by_count(self,d):  #d 傳dictionary進來，傳回排完的lis
		res=[(g.count,g) for key,g in d.items() ] # 由d 產生 key, value 交換的 tuple list
		res.sort(reverse = True)  #排序
		res=[x[1] for x in res ] #產生 key, value 交換 res中每個 tuple的內容順序，這樣看才順眼。
		return res
	
	def sort_dict_by_key(self,d):  #d 傳dictionary進來，傳回排完的lis
		res=[(g.text,g) for g in d.values() ] # 由d 產生 key, value 交換的 tuple list
		res.sort(reverse = False)  #排序
		res=[x[1] for x in res ] #產生 key, value 交換 res中每個 tuple的內容順序，這樣看才順眼。
		return res
		
	def dumpTextArray(self,max=50):
		for k in range(len(self.srcTextArray)):
			print("[%d]%s...(%d words)"%(k,self.srcTextArray[k][:max],len(self.srcTextArray[k])))
		
	def dumpGrams(self,max=50):
		for i in range(len(self.grams)): 
			dictgram=self.grams[i]
			print("[text no.%d]:"%i)
			# 只印前max個
			for g in dictgram[:max]: 
				print(g),  
			print("\n")
		
		#只印前max個
		print("[text all]:")
		for g in self.allGrams[:max]:
			print(g),
		print("\n")
	
	def hasAvoidStr(self,s,avoidStrs):
		for avstr in avoidStrs:
			if avstr in s:
				return True
		return False
	
	#s為一字串, minN,maxN gram的上下界，glbGramDict: 全部gram混合字典
	# simple Number 目前為第幾個樣本，為紀錄位置用
	# avoidStrs 為一個List, 內含所有要避免的Strings, ex: ["X","地獄",...]
	def gen_grams(self,s,minN,maxN,glbGramDict,sampleNumber,avoidStrs):   
		wc_dict = {}
		for n in range(minN,maxN+1):
			for i in range(len(s)-(n-1)):
				bg = s[i:i+n]
				if avoidStrs and self.hasAvoidStr(bg,avoidStrs):  #avoid String 出現在字串中
					continue
				if bg not in wc_dict: #本sample第一次出現
					if bg not in glbGramDict:  # 同時間，建立整合 gramDict --> self.allGramsDict
						arr=[0 for i in range(self.sampleSize)] #初始化位置矩陣
						arr[sampleNumber]=1
						glbGramDict[bg]=gram(bg,1,1,arr)
						wc_dict[bg]=gram(bg,1,0,arr) #dictionary在新增時，會自動對應已有的索引, 
					else:  #整體dict有，但是本sample 第一次出現
						glbGramDict[bg].count+=1	#count 值+1
						glbGramDict[bg].docCount+=1	#doc count 值+1
						glbGramDict[bg].pos[sampleNumber]+=1	#此location, count 值+1
						wc_dict[bg]=gram(bg,1,0,glbGramDict[bg].pos) #dictionary在新增時，會自動對應已有的索引, 
				else: #並非本sample第一次出現
					wc_dict[bg].count+=1 #count 值+1
					glbGramDict[bg].count+=1	#count 值+1
					glbGramDict[bg].pos[sampleNumber]+=1	#此location, count 值+1
					
		return wc_dict #回傳key=bigram, value=次數 的dictionary
	
	# 建立Gram 步驟0, 初始化內部參數
	def build_gram_proc_s0_init(self): 
		self.grams=[]		# 排序過之各字串之 gram List 所組成的List
		self.gramsDict=[]	# 各字串之 gramDict 所組成的List
		self.allGrams=[]  #整合字串之 gram List
		self.allgramsDict={}  # 整合字串之 gramDict
	
	# 建立Gram 步驟1, 呼叫self.gen_grams,切字串/計數
	# 產生各字串之 gramDict，並整合至 self.gramsDict (Global List)
	def build_gram_proc_s1_gen_gram(self,minGramLen,maxGramLen,verbose,avoidStrs):  
		for i in range(len(self.srcTextArray)):   # i, 因為要列印出目前狀態，所以需要i
			txt=self.srcTextArray[i]   # txt： 一個樣本的完整內容，字串
			if verbose:  
				print("正在為 text %d 建立gram ..." % i)
			d=self.gen_grams(txt,minGramLen,maxGramLen,self.allgramsDict,i,avoidStrs)
			self.gramsDict.append(d)
		
	# 建立Gram 步驟2 (ngram only), 文件數過濾
	def build_gram_proc_s2_docFilter_ngram(self,docThreshold,verbose):  
		for i in range(len(self.gramsDict)):  # 計算docThreshold
			if verbose:  
				print("正在計算/過濾gram %d.文件門檻: %d." % (i,docThreshold))
			self.gramsDict[i] = dict([(g.text,gram(g.text,g.count,self.allgramsDict[g.text].docCount,g.pos)) for g in self.gramsDict[i].values() if self.allgramsDict[g.text].docCount>=docThreshold])
		self.allgramsDict=dict([ (g.text,g) for g in self.allgramsDict.values() if g.docCount>=docThreshold ])
		
	def build_gram_proc_s2_docFilter_VLngram(self,minLen,docThreshold,verbose):  
		
		flteredGrams={}
		if verbose:  
			print("正在計算/過濾gram 文件門檻: %d." % (docThreshold))
		
		allSgByLen=self.sort_dict_by_length(self.allgramsDict)  # 把所有的gram 按照長度排
		
		for i in range(len(allSgByLen)): 
			g=allSgByLen[i]
			if (g.docCount>=docThreshold):						#若gram 超過門檻
				flteredGrams[g.text]=(g)									#加入選擇過後之dict
				for sg in self.textDecomp(g.text,minLen):		#產生所有子gram
					self.allgramsDict[sg].posSubtract(g)			#刪去次數, 刪完會自動更新 docCount, count
				
		self.allgramsDict=flteredGrams								#取代原有內容
		
		for i in range(len(self.gramsDict)):							#針對所有各經 dict
			flteredGramsX={}
			for gtxt in self.gramsDict[i]:								#取出經i 之gram
				if gtxt in flteredGrams:												#若該gram 有在global選擇之中
					flteredGramsX[gtxt]=self.gramsDict[i][gtxt]		#選起來
					flteredGramsX[gtxt].docCount=sum([1 for x in flteredGramsX[gtxt].pos if x>0])		#更新docCount
					flteredGramsX[gtxt].count=flteredGramsX[gtxt].pos[i]				#更新Count
					
			self.gramsDict[i]=flteredGramsX			#取代各經 dict內容

	# 建立Gram 步驟3 , 排序
	def build_gram_proc_s3_sort(self,sortBy,verbose):  
		for i in range(len(self.gramsDict)):   # i, 因為要列印出目前狀態，所以需要i
			if verbose:  
				print("正在依%s排序gram %d..." % (sortBy,i))
			if sortBy=="count":
				sd=self.sort_dict_by_count(self.gramsDict[i])
			else:
				sd=self.sort_dict_by_key(self.gramsDict[i])
			
			self.grams.append(sd)
			
		if verbose:  
			print("正在依%s排序總字串gram ..." % (sortBy))
			if sortBy=="count":
				self.allGrams=self.sort_dict_by_count(self.allgramsDict)
			else:
				self.allGrams=self.sort_dict_by_key(self.allgramsDict)
		
	def build_ngram(self,gramLength=2,sortBy="count",docThreshold=0,verbose=False,avoidStrs=None):
		if verbose:  
			if avoidStrs:
				print("avoid Strings : [ %s ]..." % ",".join(avoidStrs))
			else:
				print("avoid Strings : [ %s ]..." % avoidStrs)
		self.build_VLNgram(gramLength,gramLength,sortBy,docThreshold,verbose,True,avoidStrs)
		
	def build_VLNgram(self,minGramLen,maxGramLen,sortBy,docThreshold,verbose=False,fromFix=False,avoidStrs=None):
		# =============  初始化 Global Variables  =============
		self.build_gram_proc_s0_init() 
		# =============  檢查參數的合法性 =================
		if minGramLen<1:
			print("build_ngram時gramLength必須大於1")
			return -1
			
		if not (sortBy == "count" or sortBy == "key" ):
			print("build_ngram 的 sortby 參數必須為 count 或 key ")
			return -2
		
		# =============  Step1: Build Grams ==================
		self.build_gram_proc_s1_gen_gram(minGramLen,maxGramLen,verbose,avoidStrs)
		# ========  Step2: 計算文件數與門檻過濾==================
		if fromFix:
			self.build_gram_proc_s2_docFilter_ngram(docThreshold,verbose)
		else:
			self.build_gram_proc_s2_docFilter_VLngram(minGramLen,docThreshold,verbose)  # VLn-gram 過濾 only
	
		# ================  Step3: 排序 =====================
		self.build_gram_proc_s3_sort(sortBy,verbose)
		#=============================================
		
