getJingTxt -- 處理xml內容，產生純文字檔。

使用注意：
0. 試用python 2.x
1. 原始資料必須移至程式「所在的資料夾中/CBETA_Data」子資料夾內。有多少XML, 就會處裡多少。
2. XML 必須是「符合量化分析規範的xml」，後續會將這些xml也上載到bitbucket
3. 產生結果放至於：Jing_Txt_Data
4. 目前設定為一經一檔。

執行指令：
python Getjing.py
