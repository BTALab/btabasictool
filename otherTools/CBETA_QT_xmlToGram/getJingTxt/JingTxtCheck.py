# -*- coding: utf-8 -*-
# 2016.02.02 新增, 確認分卷文字與不分卷文字相同
import sys
import re
import getopt
import codecs
import os
import shutil

import difflib # 兩字串比對

def main(args):
	
	#設定輸入與輸出的位址
	JuanDataDir='Txt_Data'
	JingDataDir='Txt_Data'
	# JuanDataDir='../../../CBETA-txt/T'  #用於檢查放置於最終資料夾之分卷資料
	#JingDataDir='../../../CBETA-txt/T'
	
	if not os.path.exists(JuanDataDir):
		print("**錯誤** 分卷資料夾不存在，請確認: {}/ 必須存在".format(JuanDataDir) )
		exit(1)
		
	if not os.path.exists(JingDataDir):
		print("**錯誤** 一經一檔資料夾不存在，請確認: {}/ 必須存在".format(JingDataDir) )
		exit(1)
		
	misMatchCount=0 #加總不相符次數
	for f in sorted(os.listdir(JuanDataDir)): #不僅dir,也加以排序
		if f[0]==".":
			continue  # 解決隱藏檔不處裡的問題
		if not os.path.exists(os.path.join(JingDataDir,f)):
			print("**錯誤** {0} 對應之整經資料不存在，請確認: {1}/ 必須存在".format(f,os.path.join(JingDataDir,f)) )
			exit(1)
			#continue
			
		fullJingTxtFN=os.path.join(JingDataDir,f,f+"_000.txt")	#fullJingTxtFN: 整經文字檔案位置
		with open(fullJingTxtFN) as jing:
			jingTxt=jing.read().strip()  #整經文字
			jingTxtLen=len(jingTxt)  #整經長度
		
		
		juanTxts="" #各卷文字加總
		
		for jf in sorted(os.listdir(os.path.join(JuanDataDir,f))): #各卷文字
			if jf[-8:]=="_000.txt":  #處裡一卷一檔文字與各分卷檔文字放置於同一資料夾之處裡方式
				continue 
			with open(os.path.join(JuanDataDir,f,jf)) as juan:  #讀取各卷內容
				juanTxts+=juan.read().strip()
		
		juanTxtsLen=len(juanTxts) #各卷長度加總
				
		if  (jingTxtLen == juanTxtsLen):  #字數相等
			print("(Match) {0}({1}->{2}/*.txt): {3}/{4}".format(f,fullJingTxtFN,os.path.join(JuanDataDir,f),jingTxtLen,juanTxtsLen))
		else:  #字數不相等
			misMatchCount+=1
			print("(Unmatch) {0}({1}->{2}/*.txt): {3}/{4}".format(f,fullJingTxtFN,os.path.join(JuanDataDir,f),jingTxtLen,juanTxtsLen))
			# 請參考：https://docs.python.org/3/library/difflib.html#difflib.SequenceMatcher.get_matching_blocks
			continue
			
			#進行文字比對
			s = difflib.SequenceMatcher(None,jingTxt, juanTxtSum)
			mbs=s.get_matching_blocks()
			diffList=[]
			if len(mbs)==1:
				print ("完全相同")
			else:
				for m in mbs: #第一筆資料
					if m.a==0:
						prevPos=(m.size-1,m.size-1) #第一次 a,b match 結果的結束點 (兩者都會是0->size)
						#print ("Match a[{0.a}:{1}]=b[{0.b}:{2}]".format(m,m.a+m.size-1,m.b+m.size-1))
					else:
						diffPeriod=(prevPos[0]+1,m.a-1,prevPos[1]+1,m.b-1) #差異點
						diffList.append(diffPeriod)
						aPL= 5 if diffPeriod[0]>=5 else diffPeriod[0]
						aSL=5 if diffPeriod[1]+5 <=len(jingTxt) else len(jingTxt)-diffPeriod[1]
						bPL= 5 if diffPeriod[2]>=5 else diffPeriod[2]
						bSL=5 if diffPeriod[3]+5 <=len(juanTxtSum) else len(juanTxtSum)-diffPeriod[3]
						aDiffStr="{0}*{1}*{2}".format(jingTxt[diffPeriod[0]-aPL:diffPeriod[0]],jingTxt[diffPeriod[0]:diffPeriod[1]+1],jingTxt[diffPeriod[1]+1:diffPeriod[1]+aSL])
						bDiffStr="{0}*{1}*{2}".format(juanTxtSum[diffPeriod[2]-bPL:diffPeriod[2]],juanTxtSum[diffPeriod[2]:diffPeriod[3]+1],juanTxtSum[diffPeriod[3]+1:diffPeriod[3]+bSL])
						print("a: [{0}]{1}, b: [{2}]{3}".format(diffPeriod[0],aDiffStr,diffPeriod[2],bDiffStr))
						#print ("Diff a[{0}:{1}] != b[{2}:{3}]".format(*diffPeriod))
						print ("  **Match a[{0.a}:{1}]=b[{0.b}:{2}]".format(m,m.a+m.size-1,m.b+m.size-1))
						prevPos=(m.a+m.size-1,m.b+m.size-1) #本次結束點
						
	
	#執行完成之列印
	if (misMatchCount):
		print ("比對結果：共有{}個不相符檔案，請確認log".format(misMatchCount))
	else:
		print ("比對結果：分經分卷字數完全相符")
	
if __name__ == '__main__':
	main(sys.argv[1:])
	
	
