# -*- coding: utf-8 -*-
# 2014.01.23 搭配2014.02.13 GetJing.py之修正，增加按卷分割之功能。
# 2014.06.16 去除不需要之標點符號, 與梵文區塊 <div type="梵文">
# 2014.08.04 新增byJuanCut 的設定，若byJaun = True, 但byJuanCut=False, 卷間加--unit--
#                                                               若byJaun = True, 但byJuanCut=True, 卷間加. 各卷檔案獨立 以經號+_001 表示卷號 --

import sys
import re
import codecs
import os.path
import shutil
#os.path 在OS(電腦系統)中抓檔案的路徑。只要跟路徑有關，都可以透過os.path來處理。
from lxml import etree
if (sys.version_info.major==2):  
	reload(sys)
	sys.setdefaultencoding('utf-8')

TEI_NAMESPACE = "http://www.tei-c.org/ns/1.0"
TEI = "{%s}" % TEI_NAMESPACE
XML_NAMESPACE = "http://www.w3.org/XML/1998/namespace"
XML = "{%s}" % XML_NAMESPACE
NSMAP = {None : TEI_NAMESPACE, "xml":XML_NAMESPACE} # the default namespace (no prefix)
CRLF="\r\n"

class ClassGetJing:
	"""Get a Jing from data file"""
	#建構子的寫法 參數丟進來會放在這裡 dstDir='Data' 可以不要; self一定要留著(OO的關係)
	#_init_為py本身的function
	def __init__(self,contentUnit,inputFileName,dstDir='Data',splitByJuan=False,byJuanCut=False,byPunc=True):
		self.cUnit=contentUnit  # 可能值： "jing"(一經一卷)
		self.byJuan=splitByJuan # True/False
		self.byJuanCut=byJuanCut   # True/False
		self.byPunc=byPunc # True/False
		self.currentJuanNo=0 #slef.byJuanCut 時，計算卷號用
		self.splitToken="-unit-"
		self.ifile = inputFileName
		self.dDir = dstDir
		self.tree = etree.parse(self.ifile) #把丟進來的檔名 轉成XML tree
		self.dFileTemplate=os.path.splitext(os.path.basename(self.ifile))[0] #格式化檔名
		
		self.writeFile=None
		
	def printSettings(self): #印出設定
		print ("ifile:" + self.ifile)
		#print "dDir:" + self.dDir
		
	def printTexts(self):
		#單位決定  經: jing 
		if self.cUnit=="jing": #(一經一檔)
			nodeSet = self.tree.xpath('//t:div[@level="1"]',
		namespaces={'t': 'http://www.tei-c.org/ns/1.0'})				
		
		#以經為單位+各卷切檔
		if (self.cUnit=="jing" and self.byJuan==True and self.byJuanCut==True):
			#2014.11.14 修正，改為依開始一定開檔，
			# 遇到 <ab type="juan" subtype="open">確認是否重新關檔再開
			# 處理 <ab subtype="open"> 與 <div level="1"> 位置錯置之狀況 
			#結束後會再關檔一次
			
			if not os.path.exists(self.dDir):  #產生所需的資料夾
				os.makedirs(self.dDir)
				print("建立{0} 資料夾".format(self.dDir))
				
			self.writeFile = codecs.open(os.path.join(self.dDir,"%s_001.txt" % (self.dFileTemplate)),'wb',encoding='utf-8')
			print ("處裡：%s[juan:1]" % (os.path.join(self.dDir,"%s_001.txt" % (self.dFileTemplate)))) #狀態列印
			
			# 偵測 ab[@type='juan' and @subtype='open'] 在 div[@level=1] 之外，而導致juan1, 2 黏在一起的問題
			test_ab_open_before_leve1_xapth="//t:ab[@type='juan' and @subtype='open' and not(ancestor-or-self::t:div[@level=1])]"
			testResult= self.tree.xpath(test_ab_open_before_leve1_xapth,namespaces={'t': 'http://www.tei-c.org/ns/1.0'})
			
			if len(testResult)>0 :
				self.currentJuanNo=1
				print("Warning-- juan open before div level=1,len={}".format(len(testResult)))
				print("Try to fix, set currentJuanNo=1")
				
			self.processElement(nodeSet[0]) #重要!!
			
			self.writeFile.close() #最後關檔。
			
		else: #一般切檔, 卷不分檔（走這也可設定卷間加--unit--）
			for n in range(len(nodeSet)):
				if not os.path.exists(self.dDir):  #產生所需的資料夾
					os.makedirs(self.dDir)
					print("建立{0} 資料夾".format(self.dDir))
				#一個節點丟一個檔
				self.writeFile = codecs.open(os.path.join(self.dDir,"%s_%03d.txt" % (self.dFileTemplate,n)),'wb',encoding='utf-8')
				#所有節點丟一個檔
				self.processElement(nodeSet[n]) #重要!!

				
				print ("處裡：%s" % nodeSet[n].tag) #測試輸出節點的tag

			#檔案最下方換行兩行
			#wf.write(CRLF+CRLF)
			self.writeFile.close()		
			
		
	def getGaijiInfo(self,id):
		gInfo=self.tree.xpath('t:teiHeader/t:encodingDesc/t:charDecl/t:char[@xml:id="%s"]/t:charProp/t:value' % id[1:],
		          namespaces={'t': 'http://www.tei-c.org/ns/1.0','xml':'http://www.w3.org/XML/1998/namespace'})
		return gInfo[0].text

			
	# 取代標點
	def delPunc(self,line,byPunc):
		if byPunc == True:
			puncList = u"．：「」，。、《》？【】；—)(！『』　～〈〉…■┴├┌┘└─○"
		else:
			puncList = u"　"
		line = line.strip()
		# 從標點清單puncList逐一取出標點
		for p in puncList:
			# 將標點刪除 (取代.replace)
			line = line.replace(p,'')
		return line			
		
	def processElement(self,node): #真正的核心 
		# For Debug USE
		#print("{} :: type:{} level:{} n: {} text:{}".format(node.tag,node.get("type"), node.get("level"),node.get("n"),node.text))
		NeedProcessChild = True #預設處理子節點
		NeedProcessTail = False #預設不處理tail
		
		# 僅處理後面字元, 內容, 子元素都不處理
		#<head> 內容不處理, 僅列印後方字元
		#<byline> 作者翻譯宣告, 內容不處理, 僅列印後方資料
		#<lb> 換行符號, 空元素, 內容不處理, 僅列印後方資料
		#<ab> 預設內容不處理, 僅列印後方資料, 但下有額外處裡type的函式, 
		#            若在@type在abProcessType中定義，則會印出全部
		
		
		#只要印尾巴(</>)之前不要，尾巴(</>)之後要，的list
		OnlyTailList = \
		['{%s}head' % TEI_NAMESPACE, \
		'{%s}byline' % TEI_NAMESPACE, \
		'{%s}lb' % TEI_NAMESPACE, \
		'{%s}pb' % TEI_NAMESPACE, \
		'{%s}ab' % TEI_NAMESPACE
		]
		
		#全都要的陣列
		PrintAllList = ["{%s}div" % TEI_NAMESPACE]
		
		#ab需要處裡的範圍。
		#ab type可能性有: prose, verse, w, juan, dharani
		abProcessType=["prose","verse","juan","dharani"] 
		
		#div 有skipType
		# W, xu : 前序,後拔; 梵文: 梵文區塊(T21n1392)
		divSkipType=["W","w","xu","梵文"]
		
		#無法歸類的特別情形
		#應無此類
		
		if node.tag=='{%s}div' % TEI_NAMESPACE and node.get('level')=='1': 
		# div[@level='1'] 主結構, 不處理外面的東西
			if node.text is not None and node.text.strip():
				self.writeFile.write("%s" % self.delPunc(node.text,self.byPunc))

		
		#ab 決定哪些type要被處裡, printAll 流程
		elif node.tag=='{%s}ab' % TEI_NAMESPACE and node.get('type') in abProcessType: 
			if node.get('type') =='juan' :
				# 需要分卷換檔(@subtype=open, 卷開頭)
				if (self.cUnit=="jing" and self.byJuan==True and self.byJuanCut==True): 
					if node.get('subtype')=="open": #open 開檔
						#self.writeFile.close()
						#print("here!! -- curent JuanNo: {}, should add 1 for next".format(self.currentJuanNo))
						#print("  content:{}".format(self.delPunc(node.text,self.byPunc)))
						self.currentJuanNo+=1 #卷號遞增
						#須開啟的檔名
						file_need_to_open=os.path.join(self.dDir,"%s_%03d.txt" % (self.dFileTemplate,self.currentJuanNo))
						#print(file_need_to_open)
						#如果開啟中檔名與開啟目標不同，關現檔，開新檔
						if self.writeFile.name!=file_need_to_open:
							self.writeFile.close()
							self.writeFile = codecs.open(file_need_to_open,'wb',encoding='utf-8')
							print ("處裡：%s[juan:%d]" % (file_need_to_open,self.currentJuanNo)) #測試輸出節點的tag
					#elif node.get('subtype')=="close": #close 關檔
						#self.writeFile.close()
						
					
				elif self.byJuan == True  and node.get('subtype')=="open": # 需要分卷切割(@subtype=open, 卷開頭)
					#wf.write("%s-%s-%s" % (CRLF,self.delPunc(node.text.strip(CRLF)),CRLF)) # 輸出 -卷號- 測試用
					self.writeFile.write("%s-unit-%s" % (CRLF,CRLF)) # 輸出 -unit-
				NeedProcessChild = False
				NeedProcessTail = True
			elif	node.text is not None:
				
				self.writeFile.write("%s" % self.delPunc(node.text.strip(CRLF),self.byPunc))
				
				NeedProcessChild = True
				NeedProcessTail = True
			
		#div 有skipType
		elif node.tag=='{%s}div' % TEI_NAMESPACE and node.get('type') in divSkipType: 
			#print "%s:%s" % (node.tag,node.get('type'))
			NeedProcessChild = False
			NeedProcessTail = True
			
		elif node.tag in PrintAllList:
			#print("**{}**".format(node.text))
			if node.text is not None and node.text.strip(CRLF):
				self.writeFile.write("%s" % self.delPunc(node.text.strip(CRLF),self.byPunc))
				
			NeedProcessChild = True
			NeedProcessTail = True
				
		elif node.tag in OnlyTailList:
			NeedProcessChild = False
			NeedProcessTail = True
		
		else:
			#未match內容, 印後方字元即可, 理論上不應出現.
			self.writeFilewrite("{else}%s"% self.delPunc(node.tag+CRLF,self.byPunc))
			print("{else}%s" % self.delPunc(node.tag+CRLF,self.byPunc))  # 請由log 搜尋 {else} 便可確認是否有無法處裡之內容
			print("" )
			NeedProcessTail = True
		
		# 重要! Child 一定要放在 Tail 前面
		if NeedProcessChild:
			for c in node:
				self.processElement(c) #用遞迴處理子節點
			
		if NeedProcessTail:
			if node.tail is not None and node.tail.strip():
				self.writeFile.write("%s" % self.delPunc(node.tail.strip(CRLF),self.byPunc))


			
