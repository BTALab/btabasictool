# -*- coding: utf-8 -*-
# 2014.01.23 修正，增加按卷分割之功能。
import sys
import re
import getopt
import codecs
import os
import shutil
from time import gmtime, strftime
from lxml import etree
from GetJingClass import ClassGetJing
if (sys.version_info.major==2):  
	reload(sys)
	sys.setdefaultencoding('utf-8')

#_usage_ = "usage: -i inputFile [-o outputPlace]"
def usage():
    pass
	#print _usage_

def readArgs(argv):
	pass
	
def main(args):
	
	#設定輸入與輸出的位址
	dataDir='CBETA_Data'
	outputDir='Txt_Data' # 各經一檔，分卷檔，都放到這裡
	#dataDir='../../../CBETA_TAFxml/T' # production data source
	#outputDir='../../../CBETA-txt/T' # production result by jing
	
	
	for f in sorted(os.listdir(dataDir)): #不僅dir,也加以排序
		# Loop dictionary and get Filenames
		if f[0]==".":
			continue  # 解決隱藏檔不處裡的問題
		
		#參數說明：
		## 1. contentUnit :: 怎樣的範圍存成一個檔
		##     possible Vlaue:: jing (一經一資料夾)		
		## 2. splitByJuan :: 遇到各卷開頭標記時，是否要紀錄分隔符號 "-- unit --"
		##     possible Value:: True/False
		## 3. inputFileName :: 原始xml檔案位置
		## 4. dstDir :: 輸出文字檔存放之資料夾
		outputSubDir=f[:f.find(".")] #取得副檔名前的檔名
		#一經一檔
		GetJingObj=ClassGetJing(contentUnit="jing",inputFileName=os.path.join(dataDir,f),dstDir=os.path.join(outputDir,outputSubDir),splitByJuan=False,byJuanCut=False)
		print ("-------------------------------------------------------")
		
		GetJingObj.printSettings()
		GetJingObj.printTexts()
		#各卷一檔
		GetJingObj=ClassGetJing(contentUnit="jing",inputFileName=os.path.join(dataDir,f),dstDir=os.path.join(outputDir,outputSubDir),splitByJuan=True,byJuanCut=True)
		print ("-------------------------------------------------------")
		
		GetJingObj.printSettings()
		GetJingObj.printTexts()
		
	print ("-------------------------------------------------------")
	
	
if __name__ == '__main__':
	main(sys.argv[1:])
	
	
