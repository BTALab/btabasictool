# -*- coding: utf-8 -*-
#code reference = http://docs.python.org/release/2.6.6/
import sys
import codecs
import math
import re

# 相容於python2
if (sys.version_info.major==2):  
	reload(sys)
	sys.setdefaultencoding('utf-8')

sys.path.append("../")
import VLNgramBuilder

# 刪除標點的函式
def delPunc(line):
	puncList = u"．：「」，。、《》？【】；—)(！『』　～"
	line = line.strip()
	for p in puncList:
		line = line.replace(p,'')
	return line

#----參數設定----
CRLF = '\r\n'
#輸入需要執行txt檔案名稱
#processNo="T01n0001_juan1-2test"
processNo="txts/T01n0001_pre22juan"
#processNo="txts/T01n0026_T02n0125"

#輸入需要頻於前n個的bigram
nBigram=3300
# PCA輸出檔名 本組實驗的名稱
rName = "bigramPCA"
#num_appear=在所有卷中需出現幾卷
num_appear = 10
#numBigram=取幾個bigram
numBigram = 100

#----主程式----
fin = codecs.open('%s.txt' % processNo,'r','utf8')
#fout = codecs.open('T03n0186_test_output.txt','w','utf8')

#成為一字串,回傳一經為一字串，及每unit為一字串放入list
strings = ''  #總字串
listJuan=[]    #將每一unit的內容放入listJuan,有幾個unit則有幾個字串在list中
FArray=[]      #給PCA用的list

i=0
for line in fin:
	line = re.sub(r'\[[^\]]+\]','',delPunc(line)) #去除組字式[(禾*尤)/上/日] 及  去標點
	if line != '-unit-':
		strings += line.strip()
		listJuan.append(line.strip()) #將每一unit的內容放入listJuan, txt的內容必須是已用unit切割完成
		#fout=open('cleantxt/juan-%02d.txt' % i,'w')
		#fout.write(line)
		#fout.close()
		i+=1


# ---------------------  VLNgram 物件 使用方式 ver 0.1 (目前僅有 固定n的gram功能)------------------------------------//

# 利用[sampleText1, sampleText2, .....] 的list 來產生 VLNgram 物件 
g= VLNgramBuilder.VLNgram(listJuan)

# 檢查看看資料有沒有正確輸入, 列印長度可以用max 的值來修改
g.dumpTextArray(max=10)

## Case1: 建立VLNGram ======================================
## 製作 長度=2~10 , 按照次數排序(sortBy="count"), 不過濾gram出現的文件次數(docThreshold=1), 列出執行過程 的grams 
## minGramLen 必須1 以上
## maxGramLen 程式設定不可大於10以上
## sortBy=目前可用 "count" 或 "key" 表示依次數或依字串內容排序
## verbose=True/False :: 列出/不列出 執行過程
## docThreshold  出現在至少 docThreshold 個文件中，這gram才要計算
## avoidStrs 可以把不要的字串，取代為XX (長詞優先取代), (傳入值為陣列)
g.build_VLNgram(minGramLen=2,maxGramLen=15,sortBy="count",docThreshold=8,verbose=True)  

## 檢查看看gram的產生狀況。列印的gram數目長度可以用max 的值來修改
#g.dumpGrams(max=10)


#把id 改為 "all" 可以得到把所有樣本合併的 grams 資料結構，使用方式完全相同。
print("取回 整部長阿含經 的 Top 10 grams")
gallt20=g.sortedItems(max=20)  #取得id=all (所有樣本合併的的字串中) 前20個 gram的資料。
for gram in gallt20:
	print("(%s:%i(%i)),"%(gram.text,gram.count,gram.docCount)) 
print("\n")

gramY =   g[u"弟子"]
print("整部長阿含經的 '弟子' 出現 %d(%d) 次\n"%(gramY.count,gramY.docCount))   #取得id=all(所有樣本合併的字串中), "弟子"的次數。

print("一共產生%d個特徵gram" % len(g.sortedItems()))
