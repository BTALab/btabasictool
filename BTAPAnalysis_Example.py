"""
執行命令為:
python 程式名稱.py [n]
n=過濾門檻, 直接給數字就可以; 如果沒輸入此參數, 則取程式內之預設值
"""

import re
import os
import sys
import mdp
import numpy as np
import math
import codecs

from datetime import *	#janice add, 2014-08-19

sys.path.append("../") #可以看到BTAUtility的相對路徑

from BTAUtility import textdata
from BTAUtility import VLNgramBuilder

#ver 0.1 (2014.08.01 by Joey Hung)  BTAPv2 切割型. 主測試檔

#實驗清單定義：
#Group A { label, [ text1, text2.....textx] }
#Group B { label, [ text1, text2.....textx] }
#Group C { label, [ text1, text2.....textx] }
#text: t1F1(T1第一卷), t1J1(T1第一經), Lx-Ly(第x行到第y行), Cxxxxx(自給檔案)
#所有下載，或自我給定清單，都放在txt/ 下面

anaTextGroups=textdata.textGroups([  \
		("長阿含經",[
			 "T1F1",  #T1第一卷
			 "T1F2",
			 "T1F3",
			 "T1F4",
			 "T1F5",
			 "T1F6",
			 "T1F7",
			 "T1F8",
			 "T1F9",
			 "T1F10"
		]),
		("中阿含經",[
			 "T26F1",  #T26第一卷
			 "T26F2",
			 "T26F3",
			 "T26F4",
			 "T26F5",
			 "T26F6",
			 "T26F7",
			 "T26F8",
			 "T26F9",
			 "T26F10"
		]),
		("增一阿含經",[
			 "T125F1",  #T125第一卷
			 "T125F2",
			 "T125F3",
			 "T125F4",
			 "T125F5",
			 "T125F6",
			 "T125F7",
			 "T125F8",
			 "T125F9",
			 "T125F10"
		])
	])

print (anaTextGroups)
#for tg in anaTextGroups:
	#print ("{0}, 經數:{1}".format(tg.label,len(tg)))

lineColor=["red","blue","orange","yellow","green","black","blueviolet"]
pointType=[3,5,9,12,17]

CRLF = '\r\n'

# expLabel="BTAP-1"
expLabel = datetime.now().strftime('%y%m%d')
for tg in anaTextGroups:
	expLabel = "{0}-{1}{2}".format(expLabel,tg[0].canon,tg[0].jingNo)

num_allText= sum([len(tg) for tg in  anaTextGroups])
print  ("總經數:{0}".format(num_allText))
for gc in  anaTextGroups:
	print  ("{}群經數:{}".format(gc.label,len(gc)))

num_appear = 6
#numBigram=取幾個bigram
numBigram = None
#取VLVgram的長度
minGram = 2
maxGram = 4

# 讀外來參數(gram之過濾門檻)
num_appear = int(sys.argv[1] if len(sys.argv)>1 else num_appear)

expName="{0:s}-J-VL-G{1:d}G{2:d}-TH{3:d}".format(expLabel,minGram,maxGram,num_appear)

print ("*** %s ***"% expName)

#Output開關 開＝True  關＝False
countTxt = True      #gram的頻率與次數
gramPowerTxt = True  #gram的權重
siteXY = True        #PCA的座標

strings = ''  #總字串
listJuan=anaTextGroups.getTextDataArray()   #將資料由anaTextGroups內取出，加入listJuan中
FArray=[]      #給PCA用的list

g= VLNgramBuilder.VLNgram(listJuan)

# 檢查看看資料有沒有正確輸入, 列印長度可以用max 的值來修改
g.dumpTextArray(max=1)

## 製作 長度=2~10 , 按照次數排序(sortBy="count"), 不過濾gram出現的文件次數(docThreshold=1), 列出執行過程 的grams 
## minGramLen 必須1 以上
## maxGramLen 程式設定不可大於10以上
## sortBy=目前可用 "count" 或 "key" 表示依次數或依字串內容排序
## verbose=True/False :: 列出/不列出 執行過程
## docThreshold  出現在至少 docThreshold 個文件中，這gram才要計算
g.build_VLNgram(minGramLen=minGram,maxGramLen=maxGram,sortBy="count",docThreshold=num_appear,verbose=True, avoidStrs="X")  

## 檢查看看gram的產生狀況。列印的gram數目長度可以用max 的值來修改
#g.dumpGrams(max=10)

#把id 改為 "all" 可以得到把所有樣本合併的 grams 資料結構，使用方式完全相同。
#print("取回 整部長阿含經 的 Top 10 grams")
gallt=g.sortedItems()  #取得id=all (所有樣本合併的的字串中) 所有 gram的資料。


NArray = [] #22卷中kw的出現次數
FArray = [] #22卷中kw的出現頻率，要給PCA用
#for juanStr in listJuan:
for i in range(len(listJuan)):
	juanStr = listJuan[i]
	gram_FArray = []   #暫用接gram的出現頻率
	gram_NArray = []   #暫用接gram的出現次數
	
	for gram in gallt:
		num_kwInJuan = '%i'% gram.pos[i] #在listJuan[i]中，gram的次數
		fq_kwInJuan = float(num_kwInJuan)*100.0/len(juanStr)
		gram_NArray.append(num_kwInJuan)
		gram_FArray.append(fq_kwInJuan)
	
	NArray.append(gram_NArray)
	FArray.append(gram_FArray)

#資料儲存地
if not os.path.exists("product"):
	os.makedirs("product")


# ----- PCA運算 -----
# 設輸出維度
set_dim = 2
# 產生資料 np.float32是常數 FArray是要在np.array下使用的array 不可以直接用
data = np.array(FArray,np.float32) 
# 產生PCA節點 採用float32 同前面維度
node = mdp.nodes.PCANode(dtype='float32', output_dim=set_dim)	
# 訓練PCA節點 得到c	
node.train(data)		
#print "after MDP: " , data

# 抓取投影值 抓出轉換完的值y 放到excel畫圖
proj_data = node.execute(data)

#計算出各群最後的文件號
anaTextGroupBoundries=[ len(g) for g in anaTextGroups]
for i in range(1,len(anaTextGroupBoundries)):
		anaTextGroupBoundries[i]+=anaTextGroupBoundries[i-1]


# 輸出PCA運算結果
if siteXY == True: 
	
	fout = codecs.open('product/%s_XY.txt' % (expName),'w','utf-8')
	
	for n in range(len(proj_data)):
		if (n in anaTextGroupBoundries): #如果回圈流水號等於text Group邊界，留白
			fout.write(CRLF*2)
		fout.write("%s\t%s\t%s" % (n+1,proj_data[n][0],proj_data[n][1])+CRLF)
	fout.close() # 關檔很重要


#總解釋變異 同樣維度例子中應為100% = 1
#print u"總解釋變異:" , node.explained_variance 
#各compenent 解釋變異
#print node .d 

# 組成參數 即c1, c2, c3... 是array (輸出 node.v 即權重) 
#print node.v
#將node.v 的list重置 橫向轉直向
#print node.get_recmatrix(transposed=1)

#詞條在所有樣本的平均
#print node.avg[0]

if countTxt == True:
	
	gramcount= codecs.open('product/%s_Count.txt' % (expName),'w','utf-8')
	#for ikw in range(len(keyWords)):
	gramcount.write('Gram,X權重,Y 權重,總平均,')
	
	#印出正確的經號
	juanHeadStr="".join([ "{},,,,".format(tg.label) for tgs in anaTextGroups for tg in tgs])
	gramcount.write(juanHeadStr+CRLF)
	
	gramcount.write(',,,%s' %(',次數,頻率,X乘積,Y乘積'*num_allText) + CRLF)
	
	ikw=0
	for gram in gallt:
		gramcount.write('%s,' % gram.text)
		gramcount.write('%s,' % node.v[ikw][0])
		gramcount.write('%s,' % node.v[ikw][1])
		gramcount.write('%s,' % node.avg[0][ikw])
		# --- add by janice dong for show sum(X乘積) and sum(Y乘積)
		# sumFbyWofX = sum(((FArray[nf][ikw]-node.avg[0][ikw])*node.v[ikw][0]) for nf in range(len(FArray)))
		# sumFbyWofY = sum(((FArray[nf][ikw]-node.avg[0][ikw])*node.v[ikw][1]) for nf in range(len(FArray)))
		# gramcount.write('%s,' % sumFbyWofX)	# X乘積加總
		# gramcount.write('%s,' % sumFbyWofY)	# Y乘積加總
		#-------------------------
		for nf in range(len(FArray)):
			gramcount.write('%s,' % NArray[nf][ikw])  #次數,頻率
			gramcount.write('%s,' % FArray[nf][ikw])   #頻率
			gramcount.write('%s,' % ((FArray[nf][ikw]-node.avg[0][ikw])*node.v[ikw][0]))  #X乘積
			gramcount.write('%s,' % ((FArray[nf][ikw]-node.avg[0][ikw])*node.v[ikw][1]))  #Y乘積
		ikw += 1
		gramcount.write(CRLF)
	gramcount.close()

if gramPowerTxt == True:
	grampower=codecs.open('product/%s_gramPower.txt' % (expName),'w','utf-8')
	#利用anaTextGroups中，各群組的label, 建立Header
	grampower.write('Gram, X權重, Y權重, 總平均,'+"".join([ "{},,,,".format(g.label) for g in anaTextGroups])+CRLF)
	#利用anaTextGroups中，群組的個數, 建立Header
	grampower.write(",,,,"+"docTH,Doc% , totalCount,AVG ,"*len(anaTextGroups) + CRLF)

	iNom = 0 #幫助寫入權重用
	for gram in gallt:
		grampower.write('%s,' % gram.text)
		grampower.write('%s,' % node.v[iNom][0])
		grampower.write('%s,' % node.v[iNom][1])
		grampower.write('%s,' % node.avg[0][iNom])
		iNom += 1
		
		#利用各群最後的文件號加總正確的次數與文件數
		for i in range(len(anaTextGroupBoundries)):
			start =anaTextGroupBoundries[i-1] if i>0 else 0
			end=anaTextGroupBoundries[i]
			TxtGroupSum=sum(gram.pos[start:end])  #加總次數
			TxtGroupDocSum=sum([1 if x >0 else 0 for x in gram.pos[start:end]]) #加總文件數
		
			grampower.write('%s,' % TxtGroupDocSum)                 # 寫入gram出現群i的文件數的總和
			grampower.write('%s,' % (TxtGroupDocSum/len(anaTextGroups[i]))) # 寫入gram出現群i的文件數比率
			grampower.write('%s,' % (TxtGroupSum))  # 寫入gram出現群i的總次數
			grampower.write('%s,' % (TxtGroupSum/len(anaTextGroups[i])))  # 寫入gram出現群i的平均次數
		grampower.write(CRLF)

	grampower.close()

gplotF= codecs.open('product/%s_gnuplot.plt' % (expName),'w','utf-8')
# ----- Gnuplot 繪圖 -----	
# 此段因為python沒有 與gnuplot的溝通函式，因此改為輸出plt檔。
#----------------------------------------------------------------
#把繪圖指令寫入到檔案中
#gplotF.write('set xrange [-2.5:2.5]'+CRLF)
#gplotF.write('set yrange [2:-2]'+CRLF)

# 設定資料樣式為points(點)
gplotF.write('set style data points'+CRLF) 
# 圖示大小
gplotF.write('set pointsize 1.25'+CRLF) 
# 設定圖片標題
gplotF.write('set title "%s"' % (expName) +CRLF) 
#設定XY標籤
gplotF.write('set xlabel "The first components of PCA"'+CRLF)
gplotF.write('set ylabel "The second components of PCA"'+CRLF)
#畫出原点的坐标轴
gplotF.write('set zeroaxis'+CRLF)
gplotF.write('set key Left reverse'+CRLF)

# gplotF.write('set terminal pngcairo  enhanced font "Verdana,12"'+CRLF)
gplotF.write('set terminal png font "微軟正黑體"'+CRLF)	# for ms windows, janice
gplotF.write('set output "%s.png"' % (expName)+CRLF)

plotStr=""
for i in range(len(anaTextGroups)):
	plotStr+='"{0}_XY.txt" using 2:3 index {1}  title "{2}" pt {3} lt rgb "{4}", "{0}_XY.txt" using 2:3:1 index {1} with labels offset 1,1 notitle,'.format(expName,i,
			anaTextGroups[i].label,pointType[i%len(pointType)],lineColor[i%len(lineColor)])

"""
gplotF.write('plot "%s_XY.txt" using 2:3 index 0 title "%s" pt 3 lt rgb "red", \
               "%s_XY.txt" using 2:3:1 index 0 with labels offset 1,1 notitle, \
               "%s_XY.txt" using 2:3 index 1 title "%s" pt 5 lt rgb "blue", \
               "%s_XY.txt" using 2:3:1 index 1 with labels offset 1,1 notitle '  \
     % (expName,anaTextGroups[0].label,expName,expName,anaTextGroups[1].label,expName )+CRLF)
"""

gplotF.write("plot "+plotStr+CRLF)

# 讀入資料檔繪圖 判斷要畫幾部經
gplotF.write('set output')
gplotF.close()
#------------------------------------------------------------


fout.close()



