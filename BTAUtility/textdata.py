import re
import os
import sys
import httplib2
from . import dataStorage
#import BTAUtility.dataStorage
#ver 0.1 (2014.07.11 by Joey Hung)  啟動BTAP專案，定義雛型

#實驗清單定義：
#Group A { label, [ text1, text2.....textx] }
#Group B { label, [ text1, text2.....textx] }
#Group C { label, [ text1, text2.....textx] }
#text: t1F1(T1第一卷), t1J1(T1第一經), Lx-Ly(第x行到第y行), Cxxxxx(自給檔案)
#所有下載，或自我給定清單，都放在txt/ 下面

#https://raw.githubusercontent.com/ddbc/CBETA-txt/master/T/T0001_000.txt
#List data in a folder: json back 
#https://api.github.com/repos/ddbc/CBETA-txt/contents/T/T0001

#檔名：Txxxx.xml (4碼經號) or Txxxx_xxx.txt (4碼經號, 3碼卷號, 卷號000 = 整經)
	
	
class txGIter(object):
	""" for iterating textGroups and textGroup """
	def __init__(self, txG):
		self.txtG = txG
		self.i = -1
	def __iter__(self):
		return self
	def __next__(self):
		if self.i<len(self.txtG)-1:
			self.i += 1         
			return self.txtG[self.i]
		else:
			raise StopIteration
	
class textGroups:
	""" 文字分析之設定(內含多群組)
		設定內包含多個textGroupSettings
		 範例：[ 
		              ( labelA, [ textA1, textA2.....textAx]), 
		              ( labelB, [ textB1, textB2.....textBy]), 
		             ]	
	"""
	def __init__(self, textGroupSettings=[]):
		""" textGroups 的初始化函式
			  參數：textGroupSettings= 外來輸入的textGroup設定資料
		"""
		#內部儲存文字分析群組的資料結構
		self.textGrps=[]
		self.index=0
		
		#嘗試把設定值加入
		for tg in textGroupSettings:
			self.append(tg)
				
	def __len__(self):
		return len(self.textGrps)
				
	def  __getitem__(self,id):
		return self.textGrps[id]
	
	def __iter__(self):
		return txGIter(self)
		
	def __str__(self):
		return "TextGroupSetting:\n ({0})".format("\n  ".join(str(v) for v in self.textGrps))
	
	def append(self, textGrpData):
		""" 新增textGroup的資料設定
			  參數：外來輸入的textGroup設定資料
		      功能：確認textGroup設定資料是否為Tuple型態或textGroup物件型態
		                  並啟動對應之資料處理與內部結構新增之函式
		"""
		assert isinstance(textGrpData,(tuple,textGroup))
		if isinstance(textGrpData,tuple):
			self.addTextGroupTuple(textGrpData)
		elif isinstance(textGrpData,textUnit):
			self.addTextGroupObject(textGrpData)
		else:
			raise AssertionError("analysisGroup 初始化資料型態錯誤，不支援的物件型態%s" % type(tu))
	
	
	def addTextGroupTuple(self, textGrpTuple):
		""" 新增 tuple型態的textGroup的資料設定
			  參數： tuple型態的textGroup的設定資料
		      功能：進行基本確認(必須有一個字串與一個List)
		                  並啟動對應之資料處理與內部結構新增之函式
		"""
		#檢查是否有設定Lable
		assert isinstance(textGrpTuple[0],str) 
		#檢查是否有給定經號list
		assert isinstance(textGrpTuple[1],list) 
		
		#將設定傳給textGroup class 來產生textGroup 物件
		#並加入self.txtGrps
		self.textGrps.append(textGroup(textGrpTuple[0],textGrpTuple[1]))
		
	def addTextGroupObject(self, tgObj):
		""" 新增textGroup 物件型態的textGroup的資料設定
			  參數：textGroup 物件型態的textGroup的設定資料
		      功能：直接新增至 self.txtGrps
		"""
		self.textGrps.append(tgObj)
	
	def getTextDataArray(self):
		""" 產生運算用的文字內容
		      就是一個tu設定內容，載入到list內的一個item, 順序按照tu順序
		"""
		#進行資料整備
		self.prepareData()
		textArray=[]   #回傳用資料結構
		
		#進行路徑確認與資料擷取
		for tg in self.textGrps:
			for tu in tg:
				if not tu.localPath or not os.path.exists:
					raise FileNotFoundError("檔案出乎意料的不存在？(%s)" % tu.localPath)
				
				#進行資料擷取
				with open(tu.localPath, "rt",encoding="utf-8") as ifile:
					textArray.append(ifile.read())
		
		#print("一共{0}經, 詳細內容: \n{1}".format(len(textArray),"\n".join(t[:30] for t in textArray)))
	
		return textArray
		
	def prepareData(self):
		""" 實體資料整備
			  呼叫textGroup的資料整備函式
		 """
		for tg in self.textGrps:
			tg .prepareData()


	
class textGroup:
	""" 文字分析之單一群組
		     輸入資料應包含: 群組名稱 label 與 群組成員清單.
		     ex:  labelA, [ textA1, textA2.....textAx], 
	"""
	def __init__(self,groupLabel,members=[]):
		""" textGroup 的初始化函式
			  參數：groupLabel= 外來輸入的textGroup標籤
						  members= 外來輸入的群組成員清單
		"""
		# 此分析群組的標籤
		self.label=groupLabel
		
		#內部儲存文字分析群組的資料結構
		self.textList=[]
		
		#嘗試把設定值加入
		for mdata in members:
			self.append(mdata)
	
	def __len__(self):
		return len(self.textList)
	
	def  __getitem__(self,id):
		return self.textList[id]
	
	def __iter__(self):
		return txGIter(self)
	
	def __str__(self):
		""" 列印Group內容 """
		return "AnalysisGroup(%s)\n  %s" %(self.label,"\n  ".join(str(v) for v in self.textList))
	
	def append(self, textUnitData):
		""" 新增並解析成員經號的資料設定
			  參數：textUnitData=外來輸入的成員經號設定資料
			  功能：確認textUnitData設定資料是否為字串型態或textUnit物件型態
						  並啟動對應之資料處理與內部結構新增之函式
		"""
		assert isinstance(textUnitData,(str,textUnit))
		if isinstance(textUnitData,str):
			self.addTextUintString(textUnitData)
		elif isinstance(textUnitData,textUnit):
			self.addTextUnitObject(textUnitData)
		else:
			raise AssertionError("analysisGroup 初始化資料型態錯誤，不支援的物件型態%s" % type(tu))
	
	def addTextUintString(self, textUnitExpr):
		""" 新增str物件型態的經號資料設定
			  參數：str物件型態的經號資料
		      功能：嘗試轉換為textUnit並新增至 self.textList
		"""
		self.textList.append(textUnit(textUnitExpr))
		
	def addTextUnitObject(self, tuObj):
		""" 新增textUnit物件型態的經號資料設定
			  參數：textUnit 物件型態的經號資料
		      功能：直接新增至 self.textList
		"""
		self.textList.append(tuObj)
	
	__repr__ = __str__
	
	def prepareData(self):
		""" 實體資料整備
			  處呼叫dataStorage物件，進行分析資料整備
			  輸出：無，但失敗時會使程式結束
		 """
		ds=dataStorage.physicalDataStore()
		for u in self.textList:
			ds.prepareData(u)
	

class textUnit:
	""" 單一文字分析資料結構
	      接受五種不同的資料給定方式：
	       exp1: T1 :: (單一字元txs)+數字結束 
		   exp2: T1F1 :: (單一字元txs)+數字+F+數字 
		   exp3: T1D1 :: (單一字元txs)+數字+D+數字 
		   exp4: LxLy :: L+CBETA行號+L+數字+CBETA行號  
	       exp5: Cxxxxx :: C + 英數混合 
	"""
	def __init__(self,textUnitExpr):
		""" 物件初始化函式
			 定義用來解析五種參數型態的regx 物件
		"""
		#exp1: T1 :: (單一字元txs)+數字結束  ^(?P<canon>T|X|S)(?P<jing>\d+)$
		self.patn1_rex = re.compile("^(?P<canon>T|X|S)(?P<jing>\d+)(?P<ext>[a-z]?)$",re.VERBOSE)
		
		#exp2: T1F1 :: (單一字元txs)+數字+F+數字 ^(?P<canon>T|X|S)(?P<jing>\d+)F(?P<juan>\d+)$
		self.patn2_rex = re.compile("^(?P<canon>T|X|S)(?P<jing>\d+)(?P<ext>[a-z]?)(F|f)(?P<juan>\d+)$",re.VERBOSE)
		
		#exp3: T1D1 :: (單一字元txs)+數字+D+數字 ^(?P<canon>T|X|S)(?P<jing>\d+)D(?P<discorse>\d+)$
		self.patn3_rex = re.compile("^(?P<canon>T|X|S)(?P<jing>\d+)(?P<ext>[a-z]?)(D|d)(?P<discourse>\d+)$",re.VERBOSE)
		
		#exp4: LxLy :: L+CBETA行號+L+數字+CBETA行號  ^L(?P<Line1>)L(?P<Line2>)$
		self.patn4_rex = re.compile("^L(?P<fromLine>.+?)-L(?P<toLine>.+?)$",re.VERBOSE)
		
		#exp5: Cxxxxx :: C + 英數混合 ^C(?P<filename>.+)$
		self.patn5_rex = re.compile("^C(?P<filename>.+)",re.VERBOSE)
		
		#用來紀錄local的檔案路徑，若有紀錄也表示資料已經確定在local資料夾中
		self.localPath=""
		self.parse(textUnitExpr)
		self.label=textUnitExpr
		
	def __str__(self):
		""" 列印經號設定內容
		"""
		if self.dataType=="jing":
			return "(canon:%s - jingNo:%s%s)" %(self.canon,self.jingNo,self.ext)
		elif self.dataType=="juan":
			return "(canon:%s - jingNo:%s%s - juanNo:  %s) " % \
										(self.canon,self.jingNo,self.ext,self.juanNo)
		elif self.dataType=="discourse":
			return "(canon:%s - jingNo:%s - discourse No: %s)" \
									%(self.canon,self.jingNo,self.discourseNo)
		elif self.dataType=="line":
			return "(fromLine: %s - toLine: %s" %(self.fLineNo,self.eLineNo)	
		else:
			return "(filename: %s) Path: %s" %(self.filename,self.localPath)	
	
	def parse(self,textUnitExpr):
		""" 解析設定文字內容
		      輸入：經號設定文字
		      處裡：經設定解析後加入後端資料結構
			  輸出：無，但處理失敗會拋出exception
		 """
		if self.patn1_rex.match(textUnitExpr):
			match = self.patn1_rex.match(textUnitExpr)
			self.dataType="jing"
			self.canon=match.group("canon")
			self.jingNo=int(match.group("jing"))
			self.ext=match.group("ext")
			self.jingNoStr="{0}{1:04d}{2}".format(self.canon,self.jingNo,self.ext)
		elif self.patn2_rex.match(textUnitExpr):
			match = self.patn2_rex.match(textUnitExpr)
			self.dataType="juan"
			self.canon=match.group("canon")
			self.jingNo=int(match.group("jing"))
			self.ext=match.group("ext")
			self.jingNoStr="{0}{1:04d}{2}".format(self.canon,self.jingNo,self.ext)
			self.juanNo=int(match.group("juan"))
		elif self.patn3_rex.match(textUnitExpr):
			match = self.patn3_rex.match(textUnitExpr)
			self.dataType="discourse"
			self.canon=match.group("canon")
			self.jingNo=int(match.group("jing"))
			self.ext=match.group("ext")
			self.discourseNo=int(match.group("discourse"))
		elif self.patn4_rex.match(textUnitExpr):
			match = self.patn4_rex.match(textUnitExpr)
			self.dataType="line"
			self.fLineNo=match.group("fromLine")
			self.eLineNo=match.group("toLine")
		elif self.patn5_rex.match(textUnitExpr):
			match = self.patn5_rex.match(textUnitExpr)
			self.dataType="custom"
			self.filename=match.group("filename")
		else:
			raise ValueError('不符合格式的經卷範圍指定字串 %s' % textUnitExpr)
		return self
		
	def setLocalPath(self,path):
		""" 解析經號設定檔案之local path
		      輸入：local path 變數
		      處裡：加入內部資料結構
			  輸出：無，
			  被使用時機：dataStorage 確認資料到齊，回寫資料位置
		 """
		self.localPath=path

