import re
import os
import sys
import httplib2

sys.path.append("../") #可以看到BTAUtility的相對路徑

from BTAUtility import textdata
from BTAUtility import VLNgramBuilder

#ver 0.1 (2014.08.01 by Joey Hung)  BTAPv2 切割型. 主測試檔

#實驗清單定義：
#Group A { label, [ text1, text2.....textx] }
#Group B { label, [ text1, text2.....textx] }
#Group C { label, [ text1, text2.....textx] }
#text: t1F1(T1第一卷), t1J1(T1第一經), Lx-Ly(第x行到第y行), Cxxxxx(自給檔案)
#所有下載，或自我給定清單，都放在txt/ 下面

if __name__ == "__main__":
	anaTextGroups=textdata.textGroups([  \
		("長阿含經前17",[
			 "T1F1",  #T1第一卷
			 "T1F2",  #T1第一卷
			 "T1F3",  #T1第一卷
			 "T1F4",  #T1第一卷
			 "T1F5",  #T1第一卷
			 "T1F6",  #T1第一卷
			 "T1F7",  #T1第一卷
			 "T1F8",  #T1第一卷
			 "T1F9",  #T1第一卷
			 "T1F10",  #T1第一卷
			 "T1F11",  #T1第一卷
			 "T1F12",  #T1第一卷
			 "T1F13",  #T1第一卷
			 "T1F14",  #T1第一卷
			 "T1F15",  #T1第一卷
			 "T1F16",  #T1第一卷
			 "T1F17"  #T1第一卷
		]),
		("長阿含經後5",[
			 "T1F18",  #T1第一卷
			 "T1F19",  #T1第一卷
			 "T1F20",  #T1第一卷
			 "T1F21",  #T1第一卷
			 "T1F22"  #T1第一卷
		])
	])

	textdata=anaTextGroups.getTextDataArray()

	# ---------------------  VLNgram 物件 使用方式------------------------------------//

	# 利用[sampleText1, sampleText2, .....] 的list 來產生 VLNgram 物件 
	g= VLNgramBuilder.VLNgram(textdata)

	# 檢查看看資料有沒有正確輸入, 列印長度可以用max 的值來修改
	g.dumpTextArray(max=10)

	## Case1: 建立VLNGram ======================================
	## 製作 長度=2~10 , 按照次數排序(sortBy="count"), 不過濾gram出現的文件次數(docThreshold=1), 列出執行過程 的grams 
	## minGramLen 必須1 以上
	## maxGramLen 程式設定不可大於10以上
	## sortBy=目前可用 "count" 或 "key" 表示依次數或依字串內容排序
	## verbose=True/False :: 列出/不列出 執行過程
	## docThreshold  出現在至少 docThreshold 個文件中，這gram才要計算
	## avoidStrs 可以把不要的字串，取代為XX (長詞優先取代), (傳入值為陣列)
	
	g.build_VLNgram(minGramLen=2,maxGramLen=10,sortBy="count",docThreshold=1,verbose=True)  

	## Case2: 建立固定長度的NGram ================================
	## 一次只能指定一個長度的NGram，若要不同長度，需要清空資料重新再執行。
	## 製作 長度=4 , 按照次數排序(sortBy="count"), 不過濾gram出現的文件次數(docThreshold=1), 列出執行過程 的grams 
	## minGramLen 必須1 以上
	## maxGramLen 請設定為與minGramLen相同
	## sortBy=目前可用 "count" 或 "key" 表示依次數或依字串內容排序
	## verbose=True/False :: 列出/不列出 執行過程
	## docThreshold  出現在至少 docThreshold 個文件中，這gram才要計算
	## avoidStrs 可以把不要的字串，取代為XX (長詞優先取代), (傳入值為陣列)
	
	#g.build_VLNgram(minGramLen=2,maxGramLen=2,sortBy="count",docThreshold=1,verbose=True)  

	## 檢查看看gram的產生狀況。列印的gram數目長度可以用max 的值來修改
	#g.dumpGrams(max=10)


	#把id 改為 "all" 可以得到把所有樣本合併的 grams 資料結構，使用方式完全相同。
	print("取回 Top 20 grams")
	gallt20=g.sortedItems(max=20)  #取得 (所有樣本合併的的字串中) 前20個 gram的資料。
	for gram in gallt20:
		print("(%s:%i(%i)),"%(gram.text,gram.count,gram.docCount)) 
	print("\n")

	gramY =   g[u"弟子"]
	print("全部樣本中 '弟子' 出現 %d(%d) 次\n"%(gramY.count,gramY.docCount))   #取得id (所有樣本合併的字串中), "弟子"的次數。
	print("弟子在各文件中的出現次數{}".format(gramY.pos))
	print("一共產生%d個特徵gram" % len(g.sortedItems()))

